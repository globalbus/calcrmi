package org.calc.CalcRMI.CalcServer;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

import org.calc.CalcRMI.CalcContracts.CalculatorInterface;

public class App {
	private static final String STOP = "stop";
	private static final String RESTART = "restart";
	private static final String HELP = "help";

	public static void main(String[] args) {
		InputStream input = ClassLoader
				.getSystemResourceAsStream("security.policy");

		try (FileOutputStream write = new FileOutputStream("security.policy")) {
			byte[] read = new byte[input.available()];
			input.read(read);
			write.write(read);
			input.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.setProperty("java.security.policy", "security.policy");
		if (System.getSecurityManager() == null) {
			SecurityManager security = new RMISecurityManager();
			System.setSecurityManager(security);
		}
		App server = new App();
		server.start();
	}

	public App() {

	}

	public synchronized void start() {
		try {
			LocateRegistry.createRegistry(1099);
		} catch (Exception e) {
			System.out.println("Server failed: " + e);
			System.exit(1);
		}
		rebind();
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		String input;
		System.out.print(">");
		try {
			while (true) {

				if (reader.ready()) {

					input = reader.readLine();
					if (input.equalsIgnoreCase(RESTART))
						rebind();
					else if (input.equalsIgnoreCase(STOP)) {
						System.out.println("Exiting..");
						System.exit(0);
					} else if (input.equalsIgnoreCase(HELP))
						System.out
								.println("Supported Commands - restart, stop, help");
					else
						System.out.println("Unknown command - try help");
					System.out.print(">");
				}
				this.wait(100);
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void rebind() {
		try {
			CalculatorImpl calc = new CalculatorImpl();
			CalculatorInterface stub = (CalculatorInterface) UnicastRemoteObject
					.exportObject(calc, 0);
			Naming.rebind("Calculator", stub);
			System.out.println("Server is ready.");
		} catch (Exception e) {
			System.out.println("Server failed: " + e);
			System.exit(1);
		}
	}
}