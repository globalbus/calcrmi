package org.calc.CalcRMI.CalcServer;

import java.io.Serializable;

import org.calc.CalcRMI.CalcContracts.CalculatorInterface;
import org.calc.CalcRMI.CalcContracts.Display;



public class CalculatorImpl implements CalculatorInterface, Serializable {

	private static final long serialVersionUID = 1L;
	private Display display;
//	@Override
//	public void setDisplay(Display display) {
//		this.display=display;
//	}

	@Override
	public double Add(double a, double b) {
		return a + b;
	}

	@Override
	public double Sub(double a, double b) {
		return a - b;
	}

	@Override
	public double Mul(double a, double b) {
		return a * b;
	}

	@Override
	public double Div(double a, double b) {
		return a / b;
	}

//	@Override
//	public void PushToScreen(String text) {
//		display.setText(text);
//	}

}