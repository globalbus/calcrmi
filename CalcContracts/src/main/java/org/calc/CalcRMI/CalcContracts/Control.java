package org.calc.CalcRMI.CalcContracts;

import javax.swing.JPanel;

public interface Control {

    JPanel getPanel();
    
}