package org.calc.CalcRMI.CalcContracts;

import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTextField;

class DisplayPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField textPane;

	public DisplayPanel(JTextField textPane)
	{
		this.textPane=textPane;
		setLayout(new GridLayout(0, 1, 0, 0));
		textPane.setEditable(true);
		add(textPane);
	}
}