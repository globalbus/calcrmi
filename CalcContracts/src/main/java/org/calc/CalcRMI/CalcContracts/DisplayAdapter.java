package org.calc.CalcRMI.CalcContracts;

import java.io.Serializable;

import javax.swing.JPanel;
import javax.swing.JTextField;


public class DisplayAdapter implements Display, Serializable {
	/**
	 * 
	 */
	private JPanel panel;
	JTextField textPane;

	public DisplayAdapter(){
		textPane = new JTextField();
		panel = new DisplayPanel(textPane);
	}
	@Override
	public JPanel getPanel() {
		return panel;
	}

	@Override
	public void setText(String text) {
		textPane.setText(text);
		
	}

}