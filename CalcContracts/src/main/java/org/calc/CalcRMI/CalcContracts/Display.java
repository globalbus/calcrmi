package org.calc.CalcRMI.CalcContracts;

public interface Display extends Control {
    void setText(String text);
}