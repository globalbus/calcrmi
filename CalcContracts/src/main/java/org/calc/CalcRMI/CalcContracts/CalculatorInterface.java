package org.calc.CalcRMI.CalcContracts;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CalculatorInterface extends Remote {
    double Add(double a, double b) throws RemoteException;
    double Sub(double a, double b) throws RemoteException;
    double Mul(double a, double b) throws RemoteException;
    double Div(double a, double b) throws RemoteException;
    //void PushToScreen(String text) throws RemoteException;
    //void setDisplay(Display display) throws RemoteException;
}