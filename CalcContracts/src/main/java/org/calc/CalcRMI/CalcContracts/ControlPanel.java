package org.calc.CalcRMI.CalcContracts;

public interface ControlPanel extends Control {

	void setCalculatorInterface(CalculatorInterface calculator);
    void PushToScreen(String text);
    void setDisplay(Display display);
}