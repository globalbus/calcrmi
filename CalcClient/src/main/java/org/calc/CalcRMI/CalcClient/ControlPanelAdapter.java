package org.calc.CalcRMI.CalcClient;


import javax.swing.JPanel;

import org.calc.CalcRMI.CalcContracts.CalculatorInterface;
import org.calc.CalcRMI.CalcContracts.ControlPanel;
import org.calc.CalcRMI.CalcContracts.Display;



public class ControlPanelAdapter implements ControlPanel {
	
	private ControlPanelPanel panel;
	private Display display;

	public ControlPanelAdapter() {
		panel = new ControlPanelPanel(this);
	}
	@Override
	public void setCalculatorInterface(CalculatorInterface calculator){
		panel.setCalculatorInterface(calculator);
	}
	
	@Override
	public JPanel getPanel() {
		return panel;
	}
	@Override
	public void PushToScreen(String text) {
		display.setText(text);
	}
	@Override
	public void setDisplay(Display display) {
		this.display=display;
	}
}