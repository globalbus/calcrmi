package org.calc.CalcRMI.CalcClient;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.calc.CalcRMI.CalcContracts.CalculatorInterface;
import org.calc.CalcRMI.CalcContracts.ControlPanel;
import org.calc.CalcRMI.CalcContracts.Display;
import org.calc.CalcRMI.CalcContracts.DisplayAdapter;

public class App {

	static CalculatorInterface calc;

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		InputStream input = ClassLoader.getSystemResourceAsStream("security.policy");
		
		try(FileOutputStream write = new FileOutputStream("security.policy")){
			byte[] read = new byte[input.available()];
			input.read(read);
			write.write(read);
			input.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	
		System.setProperty("java.security.policy", "security.policy");
		if (System.getSecurityManager() == null) {
			SecurityManager security = new RMISecurityManager();
			System.setSecurityManager(security);
		}
		Logger logger = Logger.getLogger(App.class.getName());
		String dialog = JOptionPane.showInputDialog("Wpisz nazwę serwera/adres","localhost");
		try {
			calc = (CalculatorInterface) Naming
					.lookup(String.format("rmi://%s/Calculator", dialog));
		} catch (Exception e) {
			System.out.println("Client exception: " + e);
			System.exit(0);
		}
		/* Set the Nimbus look and feel */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (javax.swing.UnsupportedLookAndFeelException
				| IllegalAccessException | ClassNotFoundException
				| InstantiationException ex) {
			logger.log(java.util.logging.Level.SEVERE, null, ex);
		}

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				MainFrame mainFrame = new MainFrame();
				Display display = new DisplayAdapter();
				mainFrame.setDisplay(display);

				ControlPanel controlPanel = new ControlPanelAdapter();
				controlPanel.setDisplay(display);
				controlPanel.setCalculatorInterface(calc);
				mainFrame.setControlPanel(controlPanel);
				mainFrame.setVisible(true);

			}
		});
	}
}