package org.calc.CalcRMI.CalcClient;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.calc.CalcRMI.CalcContracts.CalculatorInterface;
import org.calc.CalcRMI.CalcContracts.ControlPanel;
class ControlPanelPanel extends JPanel{

	CalculatorInterface calculator;
	String symbol;
	public Operation operation=Operation.None;
	boolean isComma;
	double number1;
	private static final long serialVersionUID = 1L;
	private ControlPanel control;

	/**
	 * Create the panel.
	 */

	public ControlPanelPanel(ControlPanel control) {
		this.control=control;
		setLayout(new GridLayout(4, 4, 0, 0));
		JButton btnNewButton_7 = new JButton("7");
		add(btnNewButton_7);
		JButton btnNewButton_8 = new JButton("8");
		add(btnNewButton_8);
		JButton btnNewButton_9 = new JButton("9");
		add(btnNewButton_9);
		JButton btnNewButton_plus = new JButton("+");
		add(btnNewButton_plus);
		JButton btnNewButton_6 = new JButton("6");
		add(btnNewButton_6);
		JButton btnNewButton_5 = new JButton("5");
		add(btnNewButton_5);
		JButton btnNewButton_4 = new JButton("4");
		add(btnNewButton_4);
		JButton btnNewButton_minus = new JButton("-");
		add(btnNewButton_minus);
		JButton btnNewButton_3 = new JButton("3");
		add(btnNewButton_3);
		JButton btnNewButton_2 = new JButton("2");
		add(btnNewButton_2);
		JButton btnNewButton_1 = new JButton("1");
		add(btnNewButton_1);
		JButton btnNewButton_mul = new JButton("*");
		add(btnNewButton_mul);
		JButton btnNewButton_0 = new JButton("0");
		add(btnNewButton_0);
		JButton btnNewButton_comma = new JButton(",");
		add(btnNewButton_comma);
		JButton btnNewButton_eq = new JButton("=");
		add(btnNewButton_eq);
		JButton btnNewButton_div = new JButton("/");
		add(btnNewButton_div);
		btnNewButton_1.addActionListener(new ClickNumberAction());
		btnNewButton_1.setActionCommand("1");
		btnNewButton_2.addActionListener(new ClickNumberAction());
		btnNewButton_2.setActionCommand("2");
		btnNewButton_3.addActionListener(new ClickNumberAction());
		btnNewButton_3.setActionCommand("3");
		btnNewButton_4.addActionListener(new ClickNumberAction());
		btnNewButton_4.setActionCommand("4");
		btnNewButton_5.addActionListener(new ClickNumberAction());
		btnNewButton_5.setActionCommand("5");
		btnNewButton_6.addActionListener(new ClickNumberAction());
		btnNewButton_6.setActionCommand("6");
		btnNewButton_7.addActionListener(new ClickNumberAction());
		btnNewButton_7.setActionCommand("7");
		btnNewButton_8.addActionListener(new ClickNumberAction());
		btnNewButton_8.setActionCommand("8");
		btnNewButton_9.addActionListener(new ClickNumberAction());
		btnNewButton_9.setActionCommand("9");
		btnNewButton_0.addActionListener(new ClickNumberAction());
		btnNewButton_0.setActionCommand("0");
		btnNewButton_comma.addActionListener(new ClickCommaAction());
		btnNewButton_plus.addActionListener(new ClickOperationAction());
		btnNewButton_plus.setActionCommand("Add");
		btnNewButton_minus.addActionListener(new ClickOperationAction());
		btnNewButton_minus.setActionCommand("Sub");
		btnNewButton_mul.addActionListener(new ClickOperationAction());
		btnNewButton_mul.setActionCommand("Mul");
		btnNewButton_div.addActionListener(new ClickOperationAction());
		btnNewButton_div.setActionCommand("Div");
		btnNewButton_eq.addActionListener(new ClickEqualAction());
	}

	class ClickNumberAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;


		public ClickNumberAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() instanceof JButton) {
				symbol += ((JButton) e.getSource()).getText();
					control.PushToScreen(symbol);

			}
		}


	}

	class ClickOperationAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ClickOperationAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			SaveNumber();
			operation = Operation.valueOf(e.getActionCommand());
		}
	}

	class ClickEqualAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private double number2;

		public ClickEqualAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
            if (operation == Operation.None)
            {
                number1 = 0.0;
                symbol = "";
                control.PushToScreen(symbol);
            }
            else if (operation == Operation.Add)
            {
                try{
                	number2 = Double.parseDouble(symbol);
                	number1 = calculator.Add(number1, number2);
                    symbol = String.valueOf(number1);
                }
                catch(Exception ex)
                {
                	
                }
                control.PushToScreen(symbol);
            }
            else if (operation == Operation.Sub)
            {
                try{
                	number2 = Double.parseDouble(symbol);
                	number1 = calculator.Sub(number1, number2);
                    symbol = String.valueOf(number1);
                }
                catch(Exception ex)
                {
                	
                }
                control.PushToScreen(symbol);
            }
            else if (operation == Operation.Mul)
            {
                try{
                	number2 = Double.parseDouble(symbol);
                	number1 = calculator.Mul(number1, number2);
                    symbol = String.valueOf(number1);
                }
                catch(Exception ex)
                {
                	
                }
                control.PushToScreen(symbol);
            }
            else if (operation == Operation.Div)
            {
                try{
                	number2 = Double.parseDouble(symbol);
                	number1 = calculator.Div(number1, number2);
                    symbol = String.valueOf(number1);
                }
                catch(Exception ex)
                {
                	
                }
                control.PushToScreen(symbol);
            }
            isComma = false;
            operation = Operation.None;
        }
	}


	class ClickCommaAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ClickCommaAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!isComma) {
				if (symbol.equals(""))
					symbol += "0.";
				else
					symbol += ".";
				control.PushToScreen(symbol);
				isComma = true;
			}
		}
	}
	public void setCalculatorInterface(CalculatorInterface calculator) {
		this.calculator = calculator;
		symbol = "";
	}

	void SaveNumber() {
		if (operation != Operation.None)
			new ClickEqualAction();
		try {
			number1 = Double.parseDouble(symbol);
			symbol = "";
			isComma = false;
		} catch (Exception e) {

		}
		control.PushToScreen(symbol);
	}


}